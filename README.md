# IAC

## Sommaire

[[_TOC_]]

## Choix Technique

Ne connaissant pas du tout **AWS**, j'ai regardé ce qui était équivalent côté VPC et comparé avec **Google Cloud Platform** et **Azure**. En avantage, GCP permet de créer un VPC global composé de subnets régionaux et multizones. Il y a aussi un service spécifique de Load Balancing qui permet de répartir la charge suivant les zones.  
Connaissant **Ansible** et l'outil fonctionnant très bien avec **GCP**, cela restait un choix approprié.

## Schéma

![gcp-infra](./assets/gcp-infra.png)

## Utilisation

:warning: **Sourcer les variables de GCP avec `source gcp_env.sh`. Modifier si besoin le path de `service-account.json`.** :warning:

- **Playbook principal - création de l'infrastructure réseau**

  ```bash
  ansible-playbook playbooks/main_infra.yml -v
  ```

- **Playbook principal - management des web serveurs**

  ```bash
  ansible-playbook playbooks/main_web.yml -v
  ```

- **Playbook - spécifier une/des tâches(s)**

  ```
  ansible-playbook playbooks/main_infra.yml --tags "network,firewall"
  ```

- **Inventories - lister l'inventaire dynamic de GCP**

  ```bash
  ansible-inventory -i inventories/inventory.gcp.yml --graph
  ```

- **Déchiffre service-account.json**

  ```bash
  ansible-vault decrypt service-account.json --ask-vault-pass
  ```

  

## Arborescence

```bash
├── ansible
│   ├── ansible.cfg
│   ├── gcp_env.sh
│   ├── inventories
│   │   └── inventory.gcp.yml
│   ├── playbooks
│   │   ├── infra
│   │   │   ├── 1_create_network.yml
│   │   │   ├── 2_create_instance.yml
│   │   │   └── 3_manage_system.yml
│   │   ├── main_infra.yml
│   │   ├── main_web.yml
│   │   └── web-server
│   │       └── 4_install_web.yml
│   ├── roles
│   │   ├── instance
│   │   │   ├── handlers
│   │   │   │   └── main.yml
│   │   │   └── tasks
│   │   │       ├── instances.yml
│   │   │       ├── main.yml
│   │   │       └── scaling.yml
│   │   ├── network
│   │   │   ├── handlers
│   │   │   │   └── main.yml
│   │   │   └── tasks
│   │   │       ├── firewall.yml
│   │   │       ├── main.yml
│   │   │       └── network.yml
│   │   ├── system
│   │   │   ├── handlers
│   │   │   │   └── main.yml
│   │   │   └── tasks
│   │   │       ├── health_check.yml
│   │   │       ├── load_balancer_mig.yml
│   │   │       ├── load_balancer.yml
│   │   │       └── main.yml
│   │   └── web-server
│   │       ├── handlers
│   │       │   └── main.yml
│   │       └── tasks
│   │           ├── api.yml
│   │           └── main.yml
│   └── service-account.json
└── README.md
```

